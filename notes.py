[DAY 1]

# Intro to Django
	# Framework for Python, that allows faster creation of web applications.Free and Ope source.
	# Follows MVC Approach (Model View Controller)

# Configuration Setup
# Install Django  -  
	python -m pip install Django
# Check version  -	
	python -m django --version


	# Start creating project with Django framework	-	 
		django-admin startproject <projectname>
	# Call	-	
		python manage.py runserver

	# Port = 8000 by default


	# folders
		#manage.py - server
		#__init__.py  - python package
		# settings.py - configuration(installed apps, middlewares, databases etc)
		# urls.py - urlpatterns/routes
		# asgi + wsgi - WSGI stands for Web Server Gateway Interface, and ASGI stands for Asynchronous Server Gateway interface. They both specify the interface and sit in between the web server and a Python web application or framework. One of their jobs is to handle incoming requests from the client, but they go about it in different ways.


	# run ls
	# check if db.sqlite3  discussion/  manage.py*  notes.py
	# create new app
		python manage.py startapp <appname> #todolist
		# this will create a new folder

		In app todolist > views.py > add below
		from django.http import HttpResponse
			# this allows importing of necessary classes/modules/methods needed from django.http package while the import keyword defines what we are importing

		Create urls.py in todolist
		# create url patterns within todolist


		Connect todolist to main app which is in this case discussion folder>urls.py
		# Add include



		views.py = controller
		urls.py = router
		models.py = schema


NOTE: Whenever we create app, we add it to settings> installed apps
Connect <todolist> app to settings by adding it to installed apps:
# name of app - based on apps.py - name of the class inside the apps.py in the todolist app




[DAY 2]
# Django MVC

[Model]
# Each MODEL is represented by class that inherits django.db.models.Model. Each model has a number of class variables, each of which represents a database field in the model.
	# Example in models.py
	class ToDoItem(modes.Model):
	task_name = models.CharField(max_length=50) #VARCHAR
	description = models.CharField(max_length=200)
	status = models.CharField(max_length=50, default = "pending")
	date_created = models.DateTimeField("date created")

# Some field classes have required arguments. Charfield requires that you give it a maxlength





[Migration - For changes in Model]
# MakeMigrations in terminal when there are changes to MODEL
python manage.py makemigrations <nameofapp>

# Migrate to SQL Database
python manage.py sqlmigrate <nameofapp> 0001

python manage.py migrate

# BEGIN;
# --
# -- Create model ToDoItem
# --
# CREATE TABLE "todolist_todoitem" ("id" integer NOT NULL PRIMARY KEY AUTOINCREM
# ENT, "task_name" varchar(50) NOT NULL, "description" varchar(200) NOT NULL, "s
# tatus" varchar(50) NOT NULL, "date_created" datetime NOT NULL);
# COMMIT;

# + INSTALLED APPS will display in the terminal



# Python Shell
# act as command terminal
python manage.py shell

	# example import of database using python shell
	from todolist.models import ToDoItem;
	ToDoItem.objects.all()
	exit() #to close


	from django.utils import timezone;
	todoitem = ToDoItem(task_name = "Eat", description ="Dinner", date_created = timezone.now())


	# save
	todoitem.save()



Connect MySQL Database
# run follwing commands below to install packages
pip install mysql_connector
pip install mysql

OR

pip install mysql_connector mysql



Config settings>databases


Go to admin.py. Import and register models
	from .models import GroceryItem
	admin.site.register(GroceryItem)



create a template folder>index.html
# call model and template loader(for index.html)
	from .models import <className>



1. Views (request handler)
	from django.http import HttpResponse
	from .models import GroceryItem
	#to use template created
	from django.template import loader

2. Create models

3. In urls.py,  map/import views.py module
	# import
	from . import views
	# example
	urlpatterns = [
    path('',views.index, name = "index"),
    path("<int:groceryitem_id>/",views.groceryitem, name = "viewgroceryitem")
]

4. Connect app> urls.py to the main urls.py. Import the app path and include("nameofapp.urls")
Ex: path("todolist/", include("todolist.urls")),
5. Connect <name> app to settings by adding it to installed apps:
"todolist.apps.TodolistConfig",
	
6. Connect MySQL Database
	run: pip install mysql_connector mysql
	Create Database <nameofdb> in mySQL
	configure settings>databases

	DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': "name_db",
        'USER': 'root',
        'HOST': '127.0.0.1',
        'PORT': '3306',
        'OPTIONS': {
        'init_command': "SET sql_mode= 'STRICT_TRANS_TABLES'"
        }
    }
}


6. Run Migrations command
	python manage.py makemigrations <nameofapp>
	CREATE DATABASE in MySQL
	python manage.py sqlmigrate <nameofapp> 0001
	python manage.py migrate


7. Create superUser
	run: winpty python manage.py createsuperuser

	Go to admin.py. Import and register models
		from .models import GroceryItem
		admin.site.register(GroceryItem)


# https://dev.mysql.com/doc/refman/5.7/en/sql-mode.html


8. Create template folder> index.html (file)
9. Add render in views to call templates
10. Whenever there are changes in model run migrations command



[DAY 3]
# USER AUTHENTICATION
	# Authenticating user allows us verify user and provide access to necessary parts of an application
	# Has built in feature for authenticating users and even creating user with hashed passwords where other technologies require manual configuration to achieve this

# USER OBJECTS
	# comes with user model available with commonly used fields
	https://docs.djangoproject.com/en/4.1/ref/contrib/auth/
	# 


create new function in views.py to register user
# to import built in user model
from django.contrib.auth.model import User
def register(request):

config urls.py

create a template for page layout

go back to views.py and render the template


[Day 4]
# Template Inheritance
1. Create template folder from root app
	Create base.html(This is for page template and inheritance. Where we add bootstrap)

	Create navbar.html(This is where we create navbar, paste bootstrap component)

	Use template below, HTML +

	# Title
	{% block title %}To Do App{% endblock%}

	# Content
	{% block content %}
			<h1>Insert content</h1>
	{% endblock %}



Optional:{% csrf_token %} used to prevent malicious attacks, allows only 1 login across platforms. Example is in add_task.html

2. Update settings.py >Templates >'DIRS': ["templates"]

# Applying Bootstrap in Django
3. To apply bootstrap, visit https://getbootstrap.com/
	Look for "Include via CDN"

# Routes and URL Namespacing
4. In urls.py add "app_name = 'todolist' " to specify that urls listed are for todolist app.
5. Refactor all urls/redirect in templates to update 
	{login} TO 
	{{% url 'todolist:login' %}} in html file OR
	redirect("todolist:index") when it's in a python file

6.


# Django Forms
- Under your app folder<todolist> create forms.py file
	Import "from django import forms"
	Create a class for the form ex: LoginForm/Registration Form

- Create form layout by creating form.html file

- Define a function in views.py







