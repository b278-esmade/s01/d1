from django.contrib import admin

# Register your models here.

from .models import ToDoItem, Event
# need register to display the "app" in myAdmin
admin.site.register(ToDoItem)
admin.site.register(Event)
